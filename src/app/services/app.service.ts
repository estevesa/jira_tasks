import { ConnectedOverlayPositionChange } from '@angular/cdk/overlay';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { Sheet } from '../models/app.model';

@Injectable({
    providedIn: 'root'
})
export class FormService {

    private updateData = new BehaviorSubject<string>('1');

    protected url = 'http://localhost:4000';

    constructor(private readonly http: HttpClient) { }

    private httpOptions = {
        Headers: new HttpHeaders({
            'content-type': 'application/json'
        }),
    };

    public getTasks(): Observable<Sheet[]> {
        return this.http
            .get<Sheet[]>(`${this.url}/tarefas`)
                .pipe(
                    retry(2),
                    catchError(this.handleError)
                );
    }

    public saveTask(sheet: Sheet): Observable<any> {
        return this.http
            .post<any>(`${this.url}/novaTarefa`, sheet, this.httpOptions as unknown)
                .pipe(
                    retry(10),
                    catchError(this.handleError)
                );
    }

    public updateTask(id: string, sheet: Sheet): Observable<Sheet> {
        const path = `${this.url}/atualizar/tarefa/${id}`;
        return this.http
            .put<Sheet>(path, sheet)
                .pipe(
                    catchError((response: HttpErrorResponse) => throwError(response.error))
                );
    }

    public updateJiraTask(id: string): Observable<Sheet> {
        const flagJira = {
            jiraAddTask: '1'
        };
        const path = `${this.url}/atualizarJiraTask/${id}`;
        return this.http
            .put<Sheet>(path, flagJira)
                .pipe(
                    catchError((response: HttpErrorResponse) => throwError(response.error))
                );
    }

    public delTask(id: string): Observable<Sheet> {
        const path = `${this.url}/delete/tarefa/${id}`;
        return this.http
            .delete<Sheet>(path)
                .pipe(
                    catchError((response: HttpErrorResponse) => throwError(response.error))
                );
    }

    handleError(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Código do erro: ${error.status},` + `mensagem: ${error.message}`;
        }
        return throwError(errorMessage);
    }

    getUpdateData(): Observable<string> {
        return this.updateData.asObservable();
    }

    setUpdateData(update: string): void {
        this.updateData.next(update);
    }

}
