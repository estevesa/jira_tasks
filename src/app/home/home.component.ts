import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { FormDialogTasksComponent } from '../form-dialog-tasks/form-dialog-tasks.component';
import { Sheet } from '../models/app.model';
import { FormService } from '../services/app.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public retorno: string;
  public viewContainer;
  public id: string;
  public showDateHeader;
  public homeTemplate;
  public tarefa = {} as Sheet;
  public tarefas: Sheet[];

  constructor(private taskService: FormService,
              private route: ActivatedRoute,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.showDateHeader = this.route.snapshot.paramMap.get('id');
    this.route.params.subscribe((params: any) => {
      this.id = params.id;
      this.showDateHeader = params.id;
    });
    this.getData();
    this.getTasks(this.id);
  }

  public getTasks(dateReport: string): void {
    this.taskService.getTasks().subscribe((sheet: Sheet[]) => {
      this.tarefas = sheet.filter((value) => {
        const momentVariable = moment(value.startDate, 'MM/DD/YYYY');
        const stringvalue = momentVariable.format('MM/DD/YYYY');
        if ( dateReport === '1' || dateReport === null) {
        return ((value.jiraAddTask.toString() === '0'));
        } else {
          return ((value.jiraAddTask.toString() === '0') && (stringvalue.toString() === dateReport));
        }
      });
    });
  }

  public openDialog(sh: Sheet, state: string) {
    sh.state = state;
    const dialogRef = this.dialog.open(FormDialogTasksComponent, {
      width: '500px',
      height: '650px',
      data: sh,
    });
    dialogRef.afterClosed().subscribe(result => {
      this.retorno = result;
    });
  }

  public getData(): void {
    this.taskService.getUpdateData()
      .subscribe(resp => {
        if (resp) {
          this.getTasks(resp);
        }
      });
  }

}
