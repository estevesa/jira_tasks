import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { DateTime, Duration, Interval, Settings, Info, Zone } from 'luxon';

import { FormDialogTasksComponent } from '../form-dialog-tasks/form-dialog-tasks.component';
import { GridListComponent } from '../grid-list/grid-list.component';
import { Sheet } from '../models/app.model';
import { FormService } from '../services/app.service';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import * as internal from 'assert';
export interface CDDateSelect {
  date: string;
  time: string;
}
export interface ArrTime {
  hrDate: string;
  hrTime: string;
}

const FileSaver = require('file-saver');
declare var require: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  public tarefa = {} as Sheet;
  public tarefas: Array<Sheet> = [];
  public dateSelect: Array<CDDateSelect> = [];
  public date: Date = null;
  public itemComboSelected: string;
  public dateTimeFile: string;
  public retorno: string;
  public sumTasks: Array<ArrTime> = [];
  public timeSelectedSumered: any;
  public localDateTime;

  constructor(public dialog: MatDialog,
              public readonly route: ActivatedRoute,
              public readonly router: Router,
              private readonly taskService: FormService) {
                this.tarefa = {
                  id: '0',
                  ticketNo: '',
                  startDate: '',
                  timeSpent: '',
                  coment: '',
                  jiraAddTask: '',
                  dateTime: '',
                  state: 'I',
                  message: ''
                };
              }

  ngOnInit(): void {
    this.dateTimeFile = moment().format('YYYYMMDDHHMMSS');
    this.itemComboSelected = localStorage.getItem('itemComboSelected');
    this.timeSelectedSumered = localStorage.getItem('timeSelectedSumered');
    this.getDateTasks();
    this.getSumPeriod(this.itemComboSelected);
  }

  public getDownload(): void {
    this.getTasks();
  }

  public getDateTasks(): any {
    this.taskService.getTasks()
      .subscribe((sheet: Sheet[]) => {
        this.tarefas = sheet.filter(((elem) => elem.jiraAddTask.toString() === '0'));
        this.tarefas.sort((a, b) => a.startDate.localeCompare(b.startDate));
        let itemDate = '01/01/0001';
        let momentDateFormated;
        this.tarefas.forEach((vlr) => {
          momentDateFormated = moment(vlr.startDate, 'MM/DD/YYYY');
          const dateFormated = momentDateFormated.format('MM/DD/YYYY');
          const sumTime = '00:00:00';
          if (itemDate !== dateFormated) {
            itemDate = dateFormated;
            this.dateSelect.push({date: itemDate, time: sumTime});
          }
        });
      });
  }

  public getItemComboSelected(item) {
    this.itemComboSelected = item;
    localStorage.setItem('itemComboSelected', item);
    this.setDataFilter(item);
    this.router.navigate(['home', item]);
    this.getSumPeriod(item);
  }

  public setDataFilter(dataSelected: string): void {
    this.taskService.setUpdateData(dataSelected);
  }

  public getSumPeriod(dtItem: string): void {
    const dataMov = DateTime.fromObject({ years: 2022, months: 3, days: 13, hours: 0, minutes: 0, seconds: 0 },
      { zone: 'America/Sao_Paulo' });
    let dataMovAdd = dataMov.plus({ hours: 0, minutes: 0, seconds: 0 });
    this.taskService.getTasks().subscribe((sheet: Sheet[]) => {
      this.tarefas = sheet.filter((value) => {
        return Number(dtItem) === 1 ? value.jiraAddTask.toString() === '0' :
          (value.jiraAddTask.toString() === '0') && (value.startDate.slice(0, 10) === dtItem);
      });
      this.tarefas.sort((a, b) => a.startDate.localeCompare(b.startDate));
      this.tarefas.forEach((obj) => {
        if (obj.timeSpent.indexOf('h') > 0 && obj.timeSpent.indexOf('m') > 0) {
          const horasA = obj.timeSpent.slice(0, obj.timeSpent.indexOf('h'));
          const minutosA = obj.timeSpent.slice(Number(obj.timeSpent.indexOf('h')) + 1,
            Number(obj.timeSpent.indexOf('m'))).replace(/( )+/g, '');
          dataMovAdd = dataMovAdd.plus({ hours: Number(horasA), minutes: Number(minutosA) });
        } else {
          if (obj.timeSpent.indexOf('h') > 0 && obj.timeSpent.indexOf('m') === - 1) {
            const horasB = obj.timeSpent.slice(0, obj.timeSpent.indexOf('h'));
            dataMovAdd = dataMovAdd.plus({ hours: Number(horasB) });
          } else {
            if (obj.timeSpent.indexOf('h') === - 1 && obj.timeSpent.indexOf('m') > 0) {
              const minutosC = obj.timeSpent.slice(0, obj.timeSpent.indexOf('m'));
              dataMovAdd = dataMovAdd.plus({ minutes: Number(minutosC) });
            }
          }
        }
      });
      this.timeSelectedSumered = dataMovAdd.toFormat('HH:mm:ss');
    });
  }

  public openDialog(state: string): void {
    this.tarefa.state = 'I';
    const dialogRef = this.dialog.open(FormDialogTasksComponent, {
      width: '500px',
      height: '650px',
      data: this.tarefa,
    });
    dialogRef.afterClosed().subscribe(result => {
      this.retorno = result;
    });
  }

  public openDialogImported(): void {
    const dialogRef = this.dialog.open(GridListComponent, {
      width: '1400px',
      height: '700px',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.retorno = result;
    });
  }

  private getTasks(): void {
    let retorno = '';
    this.taskService.getTasks()
      .subscribe((sheet: Sheet[]) => {
        this.tarefas = sheet.filter((elem): {} => {
          return (elem.jiraAddTask.toString() === '0');
        });
        retorno = 'TicketNo, StartDate, TimeSpent, Comment\n';
        this.tarefas.forEach((vlr) => {
          retorno += `${vlr.ticketNo},${vlr.startDate},${vlr.timeSpent},${vlr.coment}\n`;
        });
        this.tarefas.map((valorAtual) => {
          return (valorAtual.jiraAddTask === '0');
        });
        this.downloadFile(retorno);
        for (const item of this.tarefas) {
          this.updateFlagJira(item.id);
        }
      });
  }

  private updateFlagJira(id: string): void {
    this.taskService.updateJiraTask(id).subscribe((response) => {
    });
  }

  private downloadFile(data: string): string {
    const blob = new Blob([data], {type: 'application/octet-stream;charset=utf-8'});
    FileSaver.saveAs(blob, 'export.csv');
    return 'concluido';
  }

}
