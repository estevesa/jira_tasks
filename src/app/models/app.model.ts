export interface Sheet {
    id: string;
    ticketNo: string;
    startDate: string;
    timeSpent: string;
    coment: string;
    jiraAddTask: string;
    dateTime: string;
    state: string;
    message: string;
}

export interface SheetResponse {
    response: string;
    Sheets: Array<Sheet>;
}
