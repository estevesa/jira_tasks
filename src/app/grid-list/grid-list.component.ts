import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Sheet } from '../models/app.model';
import { FormService } from '../services/app.service';
@Component({
  selector: 'app-grid-list',
  templateUrl: './grid-list.component.html',
  styleUrls: ['./grid-list.component.scss']
})
export class GridListComponent implements OnInit {

  tarefa = {} as Sheet;
  tarefas: Sheet[];

  constructor(private taskService: FormService,
              public dialogRef: MatDialogRef<GridListComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Sheet) {
                dialogRef.disableClose = true;
              }

  ngOnInit() {
    this.getTasks();
  }

  getTasks(): void {
    this.taskService.getTasks().subscribe((sheet: Sheet[]) => {
      this.tarefas = sheet.filter((value, sh, index) => {
        return (value.jiraAddTask.toString() === '1');
      });
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}
