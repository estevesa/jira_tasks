import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDialogTasksComponent } from './form-dialog-tasks.component';

describe('FormDialogTasksComponent', () => {
  let component: FormDialogTasksComponent;
  let fixture: ComponentFixture<FormDialogTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDialogTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDialogTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
