import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Sheet } from '../models/app.model';
import { FormService } from '../services/app.service';
import * as moment from 'moment';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-form-dialog-tasks',
  templateUrl: './form-dialog-tasks.component.html',
  styleUrls: ['./form-dialog-tasks.component.scss']
})
export class FormDialogTasksComponent implements OnInit, OnDestroy {
  public ticketNo: string;
  public startDate: string;
  public timeSpent: string;
  public coment: string;
  public retornoModal;
  public arrayTimeSheet = [];
  public standardDate: string = moment().format('MM/DD/YYYY hh:mm:ss');
  public timeSheetInclusion;

  private readonly destroy$ = new Subject<boolean>();

  constructor(public dialogRef: MatDialogRef<FormDialogTasksComponent>,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: Sheet,
              private taskService: FormService) {
                dialogRef.disableClose = true;
              }

  tarefa = {} as Sheet;
  tarefas: Sheet[];

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  ngOnInit() {
    this.retornoModal = this.dialogRef.componentInstance.data;
    switch (this.retornoModal.state) {
      case 'I':
        this.timeSheetInclusion = new FormGroup({
          id: new FormControl(''),
          ticketNo: new FormControl('RFCOBR-9999', [Validators.required]),
          startDate: new FormControl(this.standardDate, [Validators.required]),
          timeSpent: new FormControl('8h', [Validators.required]),
          coment: new FormControl('seus comentários', [Validators.required]),
          jiraAddTask: new FormControl('0'),
          state: new FormControl('I'),
        });
        break;
      case 'U':
        this.timeSheetInclusion = new FormGroup({
          id: new FormControl(this.retornoModal.id),
          ticketNo: new FormControl(this.retornoModal.ticketNo),
          startDate: new FormControl(this.retornoModal.startDate),
          timeSpent: new FormControl(this.retornoModal.timeSpent),
          coment: new FormControl(this.retornoModal.coment),
          jiraAddTask: new FormControl('0'),
          state: new FormControl('U'),
        });
        break;
      case 'D':
        this.timeSheetInclusion = new FormGroup({
          id: new FormControl(this.retornoModal.id),
          ticketNo: new FormControl(this.retornoModal.ticketNo),
          startDate: new FormControl(this.retornoModal.startDate),
          timeSpent: new FormControl(this.retornoModal.timeSpent),
          coment: new FormControl(this.retornoModal.coment),
          jiraAddTask: new FormControl('0'),
          state: new FormControl('D'),
        });
        this.timeSheetInclusion.disable();
        break;
      /*
      default:
        this.timeSheetInclusion = new FormGroup({
          id: new FormControl(''),
          ticketNo: new FormControl('RFCOBR-9999', [Validators.required]),
          startDate: new FormControl(this.standardDate, [Validators.required]),
          timeSpent: new FormControl('8h', [Validators.required]),
          coment: new FormControl('seus comentários', [Validators.required]),
          jiraAddTask: new FormControl('0'),
          state: new FormControl(''),
        });
        this.timeSheetInclusion.enable();
        break;
      */
    }
  }

  ngOnDestroy(): void {
    this.retornoModal = null;
    this.arrayTimeSheet = null;
    this.tarefa = null;
    this.tarefas = null;
    this.destroy$.next(true);
  }

  public saveTask(): void {
    const dateTime = moment().format('YYYY-MM-DD hh:mm:ss');
    const request: Sheet = {
      id: '',
      ticketNo: this.timeSheetInclusion.get('ticketNo').value,
      startDate: this.timeSheetInclusion.get('startDate').value,
      timeSpent: this.timeSheetInclusion.get('timeSpent').value,
      coment: this.timeSheetInclusion.get('coment').value,
      jiraAddTask: '0',
      dateTime: dateTime.toString(),
      state: 'I',
      message: '',
    };
    if (this.timeSheetInclusion.valid) {
      this.taskService.saveTask(request).subscribe((result) => {
          this.openSnackBar(result.message, 'close');
      });
    }
  }

  public updateTask(id: string): void {
    const dateTime = moment().format('YYYY-MM-DD hh:mm:ss');
    const request: Sheet = {
      id,
      ticketNo: this.timeSheetInclusion.get('ticketNo').value,
      startDate: this.timeSheetInclusion.get('startDate').value,
      timeSpent: this.timeSheetInclusion.get('timeSpent').value,
      coment: this.timeSheetInclusion.get('coment').value,
      jiraAddTask: this.timeSheetInclusion.get('jiraAddTask').value,
      dateTime: dateTime.toString(),
      state: 'U',
      message: '',
    };
    if (this.timeSheetInclusion.valid) {
      this.taskService.updateTask(id, request).subscribe((result) => {
        this.openSnackBar(result.message, 'close');
      });
    }
  }

  public removeTask(id: string): void {
    this.taskService.delTask(id).subscribe((result) => {
      this.openSnackBar(result.message, 'close');
    });
  }

  public onClose(): void {
    this.dialogRef.close();
    window.location.reload();
  }

}
